from math import pi
from random import randrange

def ex1Menu():
	print("Cálculo das áreas de figuras geométricas:")
	print("1. Círculo")
	print("2. Triângulo")
	print("3. Retângulo")

def calcula_area_circulo(raio):
	return pi * (raio ** 2)

def calcula_area_triangulo(base, altura):
	return base * altura / 2

def calcula_area_retangulo(base, altura):
	return base * altura

def input_matriz_aleatoria():
	linhas = int(input("Digite o número de linhas da matriz: "))
	colunas = int(input("Digite o número de colunas da matriz: "))
	intervalo_inicial = int(input("Digite o intervalo inicial da matriz: "))
	intervalo_final = int(input("Digite o intervalo final da matriz: "))
	return gera_matriz_aleatoria(linhas, colunas, intervalo_inicial, intervalo_final)

def gera_matriz_aleatoria(linhas, colunas, intervalo_inicial, intervalo_final):
	matriz = [[randrange(intervalo_final - intervalo_inicial) for i in range(colunas)] for j in range(linhas)]
	return matriz

def calcula_traco_matriz(matriz):
	traco = []
	soma = 0
	for i in range(len(matriz)):
		traco.append(matriz[i][soma])
		soma += 1
	return sum(traco)

def soma_matrizes(A, B):
	C = []
	for i in range(len(A)):
		C.append([])
		for j in range(len(A[i])):
			C[i].append(A[i][j] + B[i][j])
	return C

def multipla_matriz_por_constante(matriz, constante):
	matriz_final = []
	for i in range(len(matriz)):
		matriz_final.append([])
		for j in matriz[i]:
			matriz_final[i].append(j * constante)
	return matriz_final

def ordenar(dicionario):
	dicionario_final = {}
	for serie, protagonistas in sorted(dicionario.items()):
		protagonistas.sort()
		dicionario_final.update({serie: protagonistas})
	return dicionario_final

def obtem_dados_funcionarios():
	return {
		1: ["Ana", "F", "TI", 7, 3200],
		2: ["Beatriz", "F", "TI", 4, 3720],
		3: ["Carla", "F", "TI", 1, 2100],
		4: ["Daniela", "F", "RH", 2, 3920],
		5: ["Emílio", "M", "RH", 7, 4235.12],
		6: ["Fernando", "M", "Marketing", 7, 1200],
		7: ["Gabriela", "F", "Marketing", 8, 7234.89],
		8: ["Hernandes", "M", "TI", 6, 4234.12],
		9: ["Ítalo", "M", "RH", 13, 13934.23],
		10: ["Janaína", "F", "RH", 7, 9341.89]
	}

def obtem_dados_funcionarios_5_anos():
	dicionario = obtem_dados_funcionarios()
	apagar = []
	for cod, dados in dicionario.items():
		if not dados[3] > 5:
			apagar.append(cod)
	for cod in apagar:
		dicionario.pop(cod)
	return dicionario

def lista_mulheres_por_setor(dicionario, setor):
	apagar = []
	for cod, dados in dicionario.items():
		if dados[1] != 'F' or dados[2] != setor.upper():
			apagar.append(cod)
	for cod in apagar:
		dicionario.pop(cod)
	return dicionario

def dados_por_genero(genero):
	dicionario = obtem_dados_funcionarios()
	media = 0
	quantia = 0
	for cod, dados in dicionario.items():
		if dados[1] == genero.upper():
			media += dados[4]
			quantia += 1
	media /= quantia
	return media

