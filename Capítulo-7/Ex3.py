import MinhasFuncoes

print("********** Para a matriz A **********")
A = MinhasFuncoes.input_matriz_aleatoria()
print()

print("********** Para a matriz B **********")
B = MinhasFuncoes.input_matriz_aleatoria()
print()

if len(A) != len(B) or len(A[0]) != len(B[0]):
	print("As matrizes tem tamanhos diferentes!")
	exit(1)

C = MinhasFuncoes.soma_matrizes(A,B)

print(f"A -> {A}")
print(f"B -> {B}")
print(f"C -> {C}")
