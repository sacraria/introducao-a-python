import MinhasFuncoes

seriados = {}
while True:
	titulo = input("Digite o nome da série: ")
	seriados.update({titulo: [input("Digite o 1° protagonista: "),
	                input("Digite o 2° protagonista: ")]})
	if int(input("Deseja continuar? (* - Sim, 0 - Não): ")) == 0:
		break

seriadosAlfabeticos = MinhasFuncoes.ordenar(seriados)

print("********** Dicionário Original **********")
for titulo, protagonistas in seriados.items():
	print(f"Título -> {titulo}")
	print(f"1° protagonista -> {protagonistas[0]}")
	print(f"2° protagonista -> {protagonistas[1]}")
	print()

print("********** Dicionário Alfabético **********")
for titulo, protagonistas in seriadosAlfabeticos.items():
	print(f"Título -> {titulo}")
	print(f"1° protagonista -> {protagonistas[0]}")
	print(f"2° protagonista -> {protagonistas[1]}")
	print()
