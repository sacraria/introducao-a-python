import MinhasFuncoes

# linhas = int(input("Digite o número de linhas da matriz: "))
# colunas = int(input("Digite o número de colunas da matriz: "))
# intervalo_inicial = int(input("Digite o intervalo inicial da matriz: "))
# intervalo_final = int(input("Digite o intervalo final da matriz: "))
# matriz_aleatoria = MinhasFuncoes.gera_matriz_aleatoria(linhas, colunas, intervalo_inicial, intervalo_final)
matriz_aleatoria = MinhasFuncoes.input_matriz_aleatoria()
print(f"Matriz aleatória -> {matriz_aleatoria}")


if len(matriz_aleatoria) == len(matriz_aleatoria[0]):
	traco = MinhasFuncoes.calcula_traco_matriz(matriz_aleatoria)
	print(f"Traço -> {traco}")
else:
	print(f"A matriz não é quadrada, portanto não é possível calcular o traço")
