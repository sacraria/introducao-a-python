import MinhasFuncoes

genero = input("Digite o gênero desejado (M ou F): ")
if genero.upper() != 'M' and genero.upper() != 'F':
	print("O gênero não existe!")
	exit(1)
media = MinhasFuncoes.dados_por_genero(genero)
print(f"Média -> R${media:.2f}")
