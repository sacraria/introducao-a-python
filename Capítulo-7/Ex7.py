import MinhasFuncoes

dicionario = MinhasFuncoes.obtem_dados_funcionarios()

for cod, dados in dicionario.items():
	print(f"Código -> {cod}")
	print(f"Nome -> {dados[0]}")
	print(f"Gênero -> {dados[1]}")
	print(f"Área de atuação -> {dados[2]}")
	print(f"Tempo de serviço -> {dados[3]}")
	print(f"Salário -> {dados[4]}")
	print()

homens = 0
mulheres = 0

for cod, dados in dicionario.items():
	if dados[1] == 'M': homens += 1
	if dados[1] == 'F': mulheres += 1

print(f"Homens -> {homens}")
print(f"Mulheres -> {mulheres}")
