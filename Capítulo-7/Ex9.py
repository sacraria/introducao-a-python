import MinhasFuncoes
import MinhasFuncoes

dicionario = MinhasFuncoes.obtem_dados_funcionarios()
setor = input("Digite o setor desejado: ")
dicionario_mulheres = MinhasFuncoes.lista_mulheres_por_setor(dicionario, setor)

for cod, dados in dicionario_mulheres.items():
	print(f"Código -> {cod}")
	print(f"Nome -> {dados[0]}")
	print(f"Gênero -> {dados[1]}")
	print(f"Área de atuação -> {dados[2]}")
	print(f"Tempo de serviço -> {dados[3]}")
	print(f"Salário -> {dados[4]}")
	print()
