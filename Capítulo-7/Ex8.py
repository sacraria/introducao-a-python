import MinhasFuncoes

dicionario = MinhasFuncoes.obtem_dados_funcionarios_5_anos()

for cod, dados in dicionario.items():
	print(f"Código -> {cod}")
	print(f"Nome -> {dados[0]}")
	print(f"Gênero -> {dados[1]}")
	print(f"Área de atuação -> {dados[2]}")
	print(f"Tempo de serviço -> {dados[3]}")
	print(f"Salário -> {dados[4]}")
	print()
