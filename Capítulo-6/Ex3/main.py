voos = {}
while True:
	numeroVoo = int(input("Digite o número do voo: "))
	origem = input("Digite a origem da viagem: ")
	destino = input("Digite o destino da viagem: ")
	voos.update({numeroVoo: [origem, destino]})
	if int(input("Deseja continuar? (* - Sim, 0 - Não): ")) == 0:
		break

origNatal = 0
for numeroVoo, origDest in voos.items():
	if origDest[0].upper() == "NATAL":
		origNatal += 1

print(f"Voos com origem em natal -> {origNatal}")
