from operator import itemgetter
cliente = {}
while True:
	razaoSocial = input("Digite a razão social do cliente: ")
	gasto = float(input("Digite a quantia que o cliente gastou: "))
	cliente.update({razaoSocial: gasto})
	continuar = int(input("Deseja continuar? (* - Sim, 0 - Não): "))
	if continuar == 0: break
for chave, gasto in sorted(cliente.items(), key=itemgetter(1)):
	print(f"{chave} -> {gasto}")
