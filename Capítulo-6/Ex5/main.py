voos = {}
while True:
	numeroVoo = int(input("Digite o número do voo: "))
	origem = input("Digite a origem da viagem: ")
	destino = input("Digite o destino da viagem: ")
	voos.update({numeroVoo: [origem, destino]})
	if int(input("Deseja continuar? (* - Sim, 0 - Não): ")) == 0:
		break


if input("Deseja alterar algum voo? (* - Sim, 0 - Não): ") != 0:
	while True:
		numeroVoo = int(input("Digite o número do voo à se alterar: "))
		if numeroVoo in voos:
			origem = input("Digite a origem da viagem: ")
			destino = input("Digite o destino da viagem: ")
			voos.update({numeroVoo: [origem, destino]})
			print("Voo alterado!")
		else:
			print("Voo não encontrado!")
		if int(input("Deseja continuar? (* - Sim, 0 - Não): ")) == 0:
			break

for numeroVoo, origDest in voos.items():
	print(f"Número do voo -> {numeroVoo}")
	print(f"Origem do voo -> {origDest[0]}")
	print(f"Destino do voo -> {origDest[1]}\n")
