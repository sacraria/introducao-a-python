series = {}
while True:
	nome = input("Digite o nome da série: ")
	ator1 = input("Digite o nome do 1º ator/atriz: ")
	ator2 = input("Digite o nome do 2º ator/atriz: ")
	series.update({nome: [ator1, ator2]})
	if int(input("Continuar? (* - Sim, 0 - Não): ")) == 0: break

for nome, atores in sorted(series.items()):
	atores = sorted(atores)
	print(f"Série -> {nome}")
	print(f"Atores -> {atores[0]} e {atores[1]}\n")
