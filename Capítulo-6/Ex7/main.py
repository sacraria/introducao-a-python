series = {}
while True:
	nome = input("Digite o nome da série: ")
	ator1 = input("Digite o nome do 1º ator/atriz: ")
	ator2 = input("Digite o nome do 2º ator/atriz: ")
	series.update({nome: [ator1, ator2]})
	if int(input("Continuar? (* - Sim, 0 - Não): ")) == 0: break

while True:
	nome = input("Digite o nome de uma série para encontrála: ")
	if nome in series:
		print(f"Atores -> {series[nome][0]} e {series[nome][1]}")
	else:
		print(f"Não foi possível encontrar essa série")
	if int(input("Continuar? (* - Sim, 0 - Não): ")) == 0: break
