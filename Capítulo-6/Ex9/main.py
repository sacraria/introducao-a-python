funcionarios = {1: ["Ana", 'F', "TI", 7, 3200],
                2: ["Beatriz", 'F', "TI", 4, 3720],
                3: ["Carla", 'F', "TI", 1, 2100],
                4: ["Daniela",'F', "RH", 2, 3920],
				5: ["Emílio",'M', "RH", 7, 4235.12],
				6: ["Fernando",'M', "Marketing", 7, 1200],
				7: ["Gabriela",'F', "Marketing", 8, 7234.89],
				8: ["Hernandes",'M', "TI", 6, 4234.12],
				9: ["Ítalo",'M', "RH", 13, 13934.23],
				10: ["Janaína",'F', "RH", 7, 9341.89],}

for codigo, info in funcionarios.items():
	if info[1] == 'F' and info[4] > 3000 and info[2] == "TI":
		print(f"{info[0]} -> R${info[4]:.2f}")
