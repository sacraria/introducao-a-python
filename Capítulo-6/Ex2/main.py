imcs = {}

while True:
	nome = input("Digite o nome do aluno: ")
	peso = float(input("Digite o peso em Kg do aluno: "))
	altura = float(input("Digite a altura em metros do aluno: "))
	imcs.update({nome: peso / (altura ** 2)})

	if int(input("Continuar? (* - Sim, 0 - Não): ")) == 0:
		break

for nome, imc in sorted(imcs.items()):
	print(f"{nome} -> {imc}")
