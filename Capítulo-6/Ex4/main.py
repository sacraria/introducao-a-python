voos = {}
while True:
	numeroVoo = int(input("Digite o número do voo: "))
	origem = input("Digite a origem da viagem: ")
	destino = input("Digite o destino da viagem: ")
	voos.update({numeroVoo: [origem, destino]})
	if int(input("Deseja continuar? (* - Sim, 0 - Não): ")) == 0:
		break

recife = []
for numeroVoo, origDest in voos.items():
	if origDest[1].upper() == "RECIFE":
		recife.append(numeroVoo)

for numeroVoo in recife:
	voos.pop(numeroVoo)

if len(voos) <= 0:
	print("Não há voos sem ser pra recife")
else:
	for numeroVoo, origDest in voos.items():
		print(f"Número do voo -> {numeroVoo}")
		print(f"Origem do voo -> {origDest[0]}")
		print(f"Destino do voo -> {origDest[1]}\n")

