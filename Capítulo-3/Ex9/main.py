valorCompra = float(input("Digite o valor total da compra: "))

print("\n Escolha de acordo com a forma de pagamento:")
print("1 - à vista (em espécie)")
print("2 - cartão de débito")
print("3 - cartão de crédito (vencimento)")

escolha = int(input(">> "))

if escolha == 1:
	valorCompra *= 0.85
elif escolha == 2:
	valorCompra *= 0.9
elif escolha == 3:
	valorCompra *= 0.95
else:
	print("Escolha inválida!")
	exit(1)

print(f"O valor final é R${valorCompra:.2f}")
