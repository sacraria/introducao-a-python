estatura1 = float(input("Digite a estatura da 1ª pessoa: "))
estatura2 = float(input("Digite a estatura da 2ª pessoa: "))
estatura3 = float(input("Digite a estatura da 3ª pessoa: "))

if estatura1 == estatura2 or estatura1 == estatura3 or estatura2 == estatura3:
   print("\nHá, pelo menos, 2 pessoas com a mesma estatura")
elif estatura1 > estatura2 and estatura1 > estatura3:
   print(f"A pessoa mais alta tem {estatura1} metros")
elif estatura2 > estatura1 and estatura2 > estatura3:
   print(f"A pessoa mais alta tem {estatura2} metros")    
else:
   print(f"A pessoa mais alta tem {estatura3} metros")
