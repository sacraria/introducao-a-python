num1 = float(input("Digite o primeiro número: "))
num2 = float(input("Digite o segundo número: "))
print("")
print("1 - Média ponderada com pesos 2 e 3")
print("2 - Quadrado da soma dos números")
print("3 - Cubo do menor número")
escolha = int(input(">> "))
if escolha == 1:
	print((num1 * 2 + num2 * 3) / 5)
elif escolha == 2:
	print((num1 + num2) ** 2)
elif escolha == 3:
	if num1 < num2:
		print(num1 ** 3)
	else:
		print(num2 ** 3)
else:
	print("Escolha inválida!")
