a = float(input("Digite A: "))
b = float(input("Digite B: "))
c = float(input("Digite C: "))
delta = (b ** 2) - (4 * a * c)
if delta < 0:
	print("Não existem raízes reais")
elif delta == 0:
	print("Os resultados são os mesmos -> ", -b / (2 * a))
else:
	print("1º resultado -> ", ((-b) + (delta ** 0.5)) / (2 * a))
	print("2º resultado -> ", ((-b) - (delta ** 0.5)) / (2 * a))
