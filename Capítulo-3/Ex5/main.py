salario = float(input("Digite seu salário atual: "))
print("")
print("Escolha de acordo com o seu cargo:")
print("1 - Programador de sistemas")
print("2 - Analista de sistemas")
print("3 - Analista de banco de dados")

escolha = int(input(">> "))

if escolha == 1:
	salario *= 1.3
elif escolha == 2:
	salario *= 1.2
elif escolha == 3:
	salario *= 1.15
else:
	print("Escolha inválida! ")
	exit(1)
print(f"Seu novo salário é {salario}")
