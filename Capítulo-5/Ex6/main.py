from random import randrange

N = int(input("Digite o número referente à largura de L: "))
if N < 1:
	print("O número deve ser maior que 1!")
	exit(1)

L = [randrange(1, 51) for i in range(N)]
produto = 1
for i in L:
	produto *= i
media = produto ** (1 / N)
print(f"Média geométrica: {media}")

