quantiaTempoEspecifico = 0
media = 0
nome = []
tempo = []
for i in range(7):
	print("Digite o nome do ", i + 1, "º jogador: ", end = "")
	nome.append(input())
	print("Digite o tempo do ", i + 1, "º jogador: ", end = "")
	tempo.append(float(input()))
	if (i == 0):
		maiorTempo = i
		menorTempo = i
	else:
		if tempo[i] > tempo[maiorTempo]:
			maiorTempo = i
		if tempo[i] < menorTempo:
			menorTempo = i

media = sum(tempo) / 7
print(f"Media -> {media}")
print(f"Atleta com o menor tempo -> {nome[menorTempo]}")
print(f"Atleta com o maior tempo -> {nome[maiorTempo]}")
