from math import sqrt

lista = [1, 2, 3, 4, 5]
media = sqrt(min(lista) * max(lista))
print(f"Média geométrica entre o maior e o menor elemento -> {media}")
