from random import randrange

linhas = int(input("Informe a quantidade de linhas da matriz: "))
colunas = int(input("Informe a quantidade de colunas da matriz: "))
M = [[randrange(0, 2) for i in range(colunas)] for j in range(linhas)]

nula = True
for i in range(linhas):
	for j in range(colunas):
		if M[i][j] != 0:
			nula = False
			break

if nula:
	print("É nula")
else:
	print("Não é nula")
