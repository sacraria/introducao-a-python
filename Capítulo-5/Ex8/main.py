linhas = int(input("Digite a quantia de linhas: "))
colunas = int(input("Digite a quantia de colunas: "))

if linhas != colunas:
	print("A matriz precisa ser quadrada!")
	exit(1)
M = []
for i in range(linhas):
	x = []
	for j in range(colunas):
		x.append(int(input(f"Em M[{i}][{j}]: ")))
	M.append(x)

identidade = True
for i in range(linhas):
	for j in range(colunas):
		if i == j:
			if M[i][j] != 1:
				identidade = False
				break
		else:
			if M[i][j] != 0:
				identidade = False
				break

if identidade == True:
	print("A matriz é identidade")
else:
	print("A matriz não é identidade")
