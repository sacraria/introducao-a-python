from random import randrange

linhas = int(input("Digite a quantia de linhas: "))
colunas = int(input("Digite a quantia de colunas: "))

M = [[randrange(0, 6) for i in range(colunas)] for j in range(linhas)]

diagonal = True
zero = 0
difZero = 0
for i in range(linhas):
	for j in range(colunas):
		if M[i][j] == 0:
			zero += 1
		else:
			difZero += 1

if zero > difZero:
	print("A matriz é esparsa")
else:
	print("A matriz não é esparsa")
