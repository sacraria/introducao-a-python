listLenght = int(input("Digite um número ímpar maior que 1 referente à largura da lista: "))
if listLenght <= 1 or listLenght % 2 == 0:
	print("Número inválido")
	exit(1)

lista = []
for i in range(listLenght):
	lista.append(int(input("Digite um número inteiro positivo: ")))

elementoCentral = lista[int(len(lista)/ 2)]
fatorial = 1
for i in range(2, elementoCentral + 1):
	fatorial *= i

print(f"Fatorial de {elementoCentral} -> {fatorial}")
