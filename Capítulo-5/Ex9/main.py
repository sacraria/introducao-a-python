from random import randrange

linhas = int(input("Digite a quantia de linhas: "))
colunas = int(input("Digite a quantia de colunas: "))

if linhas != colunas:
	print("A matriz precisa ser quadrada!")
	exit(1)

M = [[randrange(0, 6) for i in range(colunas)] for j in range(linhas)]

diagonal = True
for i in range(linhas):
	for j in range(colunas):
		if i == j:
			if M[i][j] == 0:
				diagonal = False
				break
		else:
			if M[i][j] != 0:
				diagonal = False
				break

if diagonal == True:
	print("A matriz é diagonal")
else:
	print("A matriz não é diagonal")
