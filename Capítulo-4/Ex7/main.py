idade = float(input("Digite a idade do aluno: "))
menorIdade = idade
maiorIdade = idade
if idade < 0:
	print("Digite pelo menos um número positivo!")
	exit(0)
while True:
	idade = float(input("Digite a idade do aluno: "))
	if idade < 0:
		break;
	if idade > maiorIdade:
		maiorIdade = idade
	if idade < menorIdade:
		menorIdade = idade
print(f"A média da idade maior para a menor é {(maiorIdade + menorIdade) / 2}")
