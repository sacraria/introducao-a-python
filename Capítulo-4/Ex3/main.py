media = 0
for i in range(5):
	print("Digite o nome do ", i + 1, "º medicamento: ", end = "")
	nome = input()
	print("Digite o preço do ", i + 1, "º medicamento: ", end = "")
	precoMedicamento = float(input())
	media += precoMedicamento
	if i == 0:
		nomeMenor = nome
		menor = precoMedicamento
	else:
		if precoMedicamento < menor:
			nomeMenor = nome
			menor = precoMedicamento
print(f"O medicamento com o menor preço é {nomeMenor}")
media /= 5
print(f"A média de preços entre os medicamentos é {media}")
