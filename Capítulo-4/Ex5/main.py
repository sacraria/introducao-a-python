quantiaTempoEspecifico = 0
media = 0
for i in range(7):
	print("Digite o nome do ", i + 1, "º jogador: ", end = "")
	nome = input()
	print("Digite o tempo do ", i + 1, "º jogador: ", end = "")
	tempo = float(input())
	if (i == 0):
		maiorTempo = tempo
		maiorTempoNome = nome
		menorTempo = tempo
		menorTempoNome = nome
	else:
		if tempo > maiorTempo:
			maiorTempoNome = nome
			maiorTempo = tempo
		if tempo < menorTempo:
			menorTempoNome = nome
			menorTempo = tempo
	media += tempo
	if tempo >= 12 and tempo <= 15:
		quantiaTempoEspecifico += 1

media /= 7
print(f"Media -> {media}")
print(f"Atleta com o menor tempo -> {menorTempoNome}")
print(f"Atleta com o maior tempo -> {maiorTempoNome}")
print(f"Atletas com o tempo entre 12s e 15s -> {quantiaTempoEspecifico}")
