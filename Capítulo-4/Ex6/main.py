media = 0
i = 1
altura = float(input("Digite a altura do aluno: "))
menorAltura = altura
maiorAltura = altura
media += altura
while True:
	continuar = int(input("Deseja continuar? (* - sim, 0 - não): "))
	if continuar == 0:
		break;
	altura = float(input("Digite a altura do aluno: "))
	media += altura
	if altura > maiorAltura:
		maiorAltura = altura
	if altura < menorAltura:
		menorAltura = altura
	i += 1
print(f"A maior altura é {maiorAltura}")
print(f"A menor altura é {menorAltura}")
media /= i
print(f"A média das alturas é {media}")
