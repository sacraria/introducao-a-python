from math import radians, sin, cos, tan
class Ingresso:
	def __init__(self, nome_evento, valor_ingresso):
		self.nome = nome_evento
		self.valor = valor_ingresso

	def exibir_valor(self):
		return self.valor

	def __str__(self):
		return(f"{self.nome}, valor R${self.valor:.2f}")

class Retangulo:
	def __init__(self, largura, altura):
		self.largura = largura
		self.altura = altura

	def calcular_perimetro(self):
		return (self.largura * 2) + (self.altura * 2)

	def calcular_area(self):
		return self.largura * self.altura

class Ponto:
	def __init__(self,nome, x, y):
		self.nome = nome
		self.x = x
		self.y = y

	def __str__(self):
		return f"{self.nome}: {self.x}, {self.y}"

class Lista:
	def __init__(self, lista):
		self.lista = lista

	def lista_sem_repeticoes(self):
		lista_unica = []
		for item in self.lista:
			if item not in lista_unica:
				lista_unica.append(item)
		return lista_unica

class Calculadora:
	def __init__(self, num1, num2):
		self.num1 = num1
		self.num2 = num2

	def soma(self):
		return self.num1 + self.num2

	def subtracao(self):
		return self.num1 - self.num2

	def multiplicacao(self):
		return self.num1 * self.num2

	def divisao(self):
		return self.num1 / self.num2

	def potenciacao(self):
		return self.num1 ** self.num2

class Funcionario:
	def __init__(self, nome, salario):
		self.nome = nome
		self.salario = float(salario)

	def aumentar_salario(self, porcento):
		porcento /= 100
		self.salario += self.salario * porcento

class Carro:
	def __init__(self, consumo):
		self.consumo = consumo
		self.combustivel = 0

	def abastecer(self, litros):
		self.combustivel += litros

	def exibir_combustivel(self):
		return self.combustivel

	def andar(self, Kms):
		self.combustivel -= Kms * self.consumo

class ContaInvestimento:
	def __init__(self, saldo_inicial, taxa_juros):
		taxa_juros /= 100
		self.saldo_inicial = saldo_inicial
		self.taxa_juros = taxa_juros
		self.saldo_com_juros = saldo_inicial

	def adicione_juros(self):
		self.saldo_com_juros *= 1 + self.taxa_juros

class Trigonometria:

	def __init__(self, angulo):
		self.angulo_graus = angulo
		self.angulo_radianos = radians(angulo)

	def seno(self):
		return sin(self.angulo_radianos)

	def cosseno(self):
		return cos(self.angulo_radianos)

	def tangente(self):
		return tan(self.angulo_radianos)

	def __str__(self):
		return f"Graus -> {self.angulo_graus}\n" \
			f"Radianos -> {self.angulo_radianos}\n" \
			f"Seno -> {self.seno()}\n" \
			f"Cosseno -> {self.cosseno()}\n" \
			f"Tangente -> {self.tangente()}"

class Ponto10:
	def __init__(self,nome, x, y):
		self.nome = nome
		self.x = x
		self.y = y

	def calcular_distancia(self, ponto_2):
		from math import sqrt
		return sqrt((self.x - ponto_2.x) ** 2 + (self.y - ponto_2.y) ** 2)

	def __str__(self):
		return f"{self.nome}: {self.x}, {self.y}"
