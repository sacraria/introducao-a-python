from classes import Ponto10

p = Ponto10('p', 1, 1)
q = Ponto10('q', 2, 1)
r = Ponto10('r', 2, 3)
s = Ponto10('s', 3, -2)
print(p)
print(q)
print(r)
print(s)
print("Distância entre p e q -> ", p.calcular_distancia(q))
print("Distância entre q e r -> ", q.calcular_distancia(r))
print("Distância entre p e s -> ", p.calcular_distancia(s))
print("Distância entre s e p -> ", s.calcular_distancia(p))
