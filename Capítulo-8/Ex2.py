from classes import Retangulo

largura = float(input("Digite o valor da largura retângulo: "))
altura = float(input("Digite o valor da altura retângulo: "))
enzo = Retangulo(largura, altura)
print(f"perímetro -> {enzo.calcular_perimetro()}")
print(f"área -> {enzo.calcular_area()}")
