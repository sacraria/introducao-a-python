from classes import Funcionario

roger = Funcionario("Róger", 1000)
josilda = Funcionario("Josilda", 2000)
roger.aumentar_salario(20)
josilda.aumentar_salario(50)
print(f"Salário do róger -> {roger.salario}")
print(f"Salário da Josilda -> {josilda.salario}")
