from classes import Calculadora

calculadoralegal = Calculadora(0, 0)

while True:
	calculadoralegal.num1 = float(input("Digite o 1° número: "))
	calculadoralegal.num2 = float(input("Digite o 2° número: "))
	print()
	print("1 - Soma")
	print("2 - Subtração")
	print("3 - Multiplicação")
	print("4 - Divisão")
	print("5 - Potênciação")
	operacao = int(input(">> "))
	match operacao:
		case 1:
			print(calculadoralegal.soma())
		case 2:
			print(calculadoralegal.subtracao())
		case 3:
			print(calculadoralegal.multiplicacao())
		case 4:
			print(calculadoralegal.divisao())
		case 5:
			print(calculadoralegal.potenciacao())
		case _:
			print("Escolha inválida!")
	if int(input("Deseja continuar? (* - Sim, 0 - Não): ")) == 0: break
