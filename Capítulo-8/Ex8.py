from classes import ContaInvestimento

investimento = ContaInvestimento(1000.0, 10)
for i in range(5):
	investimento.adicione_juros()

print(f"Conta final -> R${investimento.saldo_com_juros:.2f}")
